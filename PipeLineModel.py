from PySide2.QtGui import QStandardItemModel, QStandardItem
from PySide2.QtWidgets import QFrame, QComboBox, QHBoxLayout, QVBoxLayout, QTreeView, QAbstractItemView, QPushButton, \
    QLabel, QSpacerItem, QSizePolicy
from PySide2.QtWidgets import QStyledItemDelegate, QStyle, QStyleOptionButton, QApplication, QItemDelegate, \
    QStackedWidget
from PySide2.QtCore import Qt, QEvent, Signal
from Dl_DataFrame import Dl_DataFrameCatalog, Dl_DataFrameFeatureSelectionModel, Dl_DataFrameMissingValuesFillModel, \
    Dl_StandardizationModel, Dl_TrainerModel, Dl_EvaluationMethodModel
from Dl_Trainer import Trainer
from Dl_ValueStandardizer import Standardizer
import ast
import pandas as pd
from enum import Enum

class ComboDelegate(QItemDelegate):
    """
    A delegate that places a fully functioning QComboBox in every
    cell of the column to which it's applied
    """

    def __init__(self, parent):

        QItemDelegate.__init__(self, parent)

    def paint(self, painter, option, index):

        if index.data(Qt.UserRole + 1) is None:
            combo = QComboBox(self.parent())
            combo.setProperty("QModelIndex", index)
            combo.currentIndexChanged.connect(self.currentIndexChanged)
            index.model().setData(index, combo, Qt.UserRole + 1)

            jsvalues = ast.literal_eval(index.data())
            combo.addItems(list(jsvalues))

            if not self.parent().indexWidget(index):
                self.parent().setIndexWidget(
                    index,
                    combo
                )

    def currentIndexChanged(self, index: int):
        combo = self.sender()


class ButtonDelegate(QStyledItemDelegate):
    buttonClicked = Signal(int)

    def __init__(self, parent=None):
        super(ButtonDelegate, self).__init__(parent)
        self._pressed = None
        self.buttonClicked.connect(self.pressed)

    def pressed(self, pressed: int):
        print("Pressed")

    def paint(self, painter, option, index):
        painter.save()
        opt = QStyleOptionButton()
        opt.text = str(index.data())
        opt.rect = option.rect
        opt.palette = option.palette
        if self._pressed and self._pressed == (index.row(), index.column()):
            opt.state = QStyle.State_Enabled | QStyle.State_Sunken
        else:
            opt.state = QStyle.State_Enabled | QStyle.State_Raised
        QApplication.style().drawControl(QStyle.CE_PushButton, opt, painter)
        painter.restore()

    def editorEvent(self, event, model, option, index):
        if event.type() == QEvent.MouseButtonPress:
            # store the position that is clicked
            self._pressed = (index.row(), index.column())
            return True
        elif event.type() == QEvent.MouseButtonRelease:
            if self._pressed == (index.row(), index.column()):
                # we are at the same place, so emit
                self.buttonClicked.emit(*self._pressed)
            elif self._pressed:
                # different place.
                # force a repaint on the pressed cell by emitting a dataChanged
                # Note: This is probably not the best idea
                # but I've yet to find a better solution.
                oldIndex = index.model().index(*self._pressed)
                self._pressed = None
                index.model().dataChanged.emit(oldIndex, oldIndex)
            self._pressed = None
            return True
        else:
            # for all other cases, default action will be fine
            return super(ButtonDelegate, self).editorEvent(event, model, option, index)


class PipeLineProcessPayload:
    def __init__(self, path="", data_frame=None, target="", model=None, evaluation_method=None):
        self.path = path
        self.data_frame = data_frame
        self.target = target
        self.model = model
        self.evaluation_method = evaluation_method

    def set_path(self, path):
        self.path = path

    def set_data_frame(self, data_frame):
        self.data_frame = data_frame

    def set_target(self, target):
        self.target = target

    def set_model(self, model):
        self.model = None

    def set_evaluation_method(self, evaluation_method):
        self.evaluation_method = evaluation_method


class PipeLineProcess:
    class Status(Enum):
        Initial = 0
        Ready = 1
        Running = 2
        Error = 3
        Ok = 4

    def __init__(self, pipeline):
        self._processType = self.__class__.__name__
        self._pipeLine = pipeline
        self._data_frame_id = -1
        self.status = PipeLineProcess.Status.Initial
        self._gui_frame = None


    def construct_gui(self):
        pass

    def set_process_type(self, t_type: str):
        self._processType = t_type

    def get_process_type(self) -> str:
        return self._processType

    def set_data_frame_id(self, idd: int):
        self._data_frame_id = idd

    def gui_frame(self) -> QFrame:
        return self._gui_frame


class DataImport(PipeLineProcess):
    def __init__(self, pipeline):
        PipeLineProcess.__init__(self, pipeline)
        super().set_process_type(self.__class__.__name__)
        self.construct_gui()

    def construct_gui(self):
        self._gui_frame = QFrame()
        self._gui_frame.setLayout(QVBoxLayout())
        select_data_frame = QFrame(self._gui_frame)
        select_data_frame.setLayout(QHBoxLayout())
        self._gui_frame.layout().addWidget(select_data_frame)

        data_select_label = QLabel("Select Data:", select_data_frame)
        select_data_combobox = QComboBox(select_data_frame)
        select_data_combobox.currentIndexChanged.connect(lambda idd: self._pipeLine.set_data_frame_id(idd))
        select_data_combobox.setModel(Dl_DataFrameCatalog.importedFileModel)
        select_data_frame.layout().addWidget(data_select_label)
        select_data_frame.layout().addWidget(select_data_combobox)
        select_data_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Expanding, QSizePolicy.Minimum))

        self._gui_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

    def apply(self, input: PipeLineProcessPayload) -> PipeLineProcessPayload:
        input.set_path(Dl_DataFrameCatalog.get_path_by_name(self._select_data_combobox.currentText()))
        input.set_data_frame(pd.read_csv(input.path))
        return input


class MissingValuesFill(PipeLineProcess):
    def __init__(self, pipeline):
        PipeLineProcess.__init__(self, pipeline)
        super().set_process_type(self.__class__.__name__)
        self.construct_gui()

    def construct_gui(self):
        self._gui_frame = QFrame()
        self._gui_frame.setLayout(QVBoxLayout())
        missing_values_fill_list_view = QTreeView(self._gui_frame)
        missing_values_fill_list_view.header().setDefaultAlignment(Qt.AlignCenter)

        missing_values_fill_model = Dl_DataFrameMissingValuesFillModel()

        missing_values_fill_list_view.setModel(missing_values_fill_model)
        missing_values_fill_list_view.setItemDelegateForColumn(4, ComboDelegate(missing_values_fill_list_view))

        missing_values_fill_list_view.setItemDelegateForColumn(5, ButtonDelegate(missing_values_fill_list_view))
        self._gui_frame.layout().addWidget(missing_values_fill_list_view)
        self._gui_frame.setProperty("missing_values_fill_model", missing_values_fill_model)

    def set_data_frame_id(self, idd: int):
        super().set_data_frame_id(idd)
        self._gui_frame.property("missing_values_fill_model").set_data_frame_id(idd)


class TargetValueSelection(PipeLineProcess):
    def __init__(self, pipeline):
        PipeLineProcess.__init__(self, pipeline)
        super().set_process_type(self.__class__.__name__)
        self._featureModel = Dl_DataFrameFeatureSelectionModel()
        self.construct_gui()

    def construct_gui(self):
        self._gui_frame = QFrame()

        self._gui_frame.setLayout(QVBoxLayout())
        select_data_frame = QFrame(self._gui_frame)
        select_data_frame.setLayout(QHBoxLayout())
        self._gui_frame.layout().addWidget(select_data_frame)

        data_select_label = QLabel("Select Target Feature:", select_data_frame)
        select_data_combobox = QComboBox(select_data_frame)
        select_data_combobox.setModel(self._featureModel)
        select_data_frame.layout().addWidget(data_select_label)
        select_data_frame.layout().addWidget(select_data_combobox)
        select_data_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Expanding, QSizePolicy.Minimum))

        self._gui_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

    def set_data_frame_id(self, idd: int):
        super().set_data_frame_id(idd)
        self._featureModel.set_data_frame_id(idd)

    def apply(self, input: PipeLineProcessPayload) -> PipeLineProcessPayload:
        input.set_target(self._select_data_combobox.currentText())
        return input


class FeatureSelection(PipeLineProcess):
    def __init__(self, pipeline):
        PipeLineProcess.__init__(self, pipeline)
        super().set_process_type(self.__class__.__name__)
        self._featureModel = Dl_DataFrameFeatureSelectionModel()
        self.construct_gui();

    def construct_gui(self):
        self._gui_frame = QFrame()
        self._gui_frame.setLayout(QVBoxLayout())
        feature_list_view = QTreeView(self._gui_frame)

        feature_list_view.setModel(self._featureModel)
        feature_list_view.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self._gui_frame.layout().addWidget(feature_list_view)
        activation_frame = QFrame(self._gui_frame)
        activation_frame.setLayout(QHBoxLayout())
        activation_button = QPushButton("Activate Selected", activation_frame)
        activation_button.clicked.connect(lambda clicked: self.set_selected_active(Qt.Checked))
        de_activation_button = QPushButton("Deactivate Selected", activation_frame)
        de_activation_button.clicked.connect(lambda clicked: self.set_selected_active(Qt.Unchecked))

        activation_frame.layout().addWidget(activation_button)
        activation_frame.layout().addWidget(de_activation_button)

        self._gui_frame.layout().addWidget(activation_frame)

    def set_selected_active(self, active_state: Qt.CheckState):
        selected_list = self._feature_list_view.selectionModel().selectedIndexes()

        for model_index in selected_list:
            model_item = self._featureModel.item(model_index.row(), 0)
            model_item.setCheckState(active_state)

    def get_selected_features(self) -> list:
        feature_list = []
        for index in range(self._featureModel.rowCount()):
            active_item = self._featureModel.item(index, 0)
            if active_item.checkState() == Qt.Checked:
                feature_name_item = self._featureModel.item(index, 0)
                feature_list.append(feature_name_item.text())

    def set_data_frame_id(self, idd: int):
        super().set_data_frame_id(idd)
        self._featureModel.set_data_frame_id(idd)

    def apply(self, input: PipeLineProcessPayload) -> PipeLineProcessPayload:
        return input


class Standardization(PipeLineProcess):
    def __init__(self, pipeline):
        PipeLineProcess.__init__(self, pipeline)
        super().set_process_type(self.__class__.__name__)
        self._type = None
        super().set_process_type(self.__class__.__name__)
        self._gui_frame = QFrame()
        self._gui_frame.setLayout(QVBoxLayout())
        self._standardization_type_frame = QFrame(self._gui_frame)
        self._standardization_type_frame.setLayout(QHBoxLayout())
        self._gui_frame.layout().addWidget(self._standardization_type_frame)

        self._standardization_method_type_label = QLabel("Standardization Type:", self._standardization_type_frame)
        self._standardization_frame_combobox = QComboBox(self._standardization_type_frame)
        self._standardization_frame_combobox.setModel(Dl_StandardizationModel())
        self._standardization_type_frame.layout().addWidget(self._standardization_method_type_label)
        self._standardization_type_frame.layout().addWidget(self._standardization_frame_combobox)
        self._standardization_type_frame.layout().addItem(
            QSpacerItem(20, 40, QSizePolicy.Expanding, QSizePolicy.Minimum))

        self._gui_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

    def get_standardization_type(self) -> Standardizer.Type:
        return self._standardization_frame_combobox.currentIndex()


class ModelTraining(PipeLineProcess):
    def __init__(self, pipeline):
        self._data = Dl_TrainerModel()
        PipeLineProcess.__init__(self, pipeline)
        super().set_process_type(self.__class__.__name__)
        self._type = None
        self.construct_gui()

    def construct_gui(self):
        self._gui_frame = QFrame()
        self._gui_frame.setLayout(QVBoxLayout())
        trainer_type_frame = QFrame(self._gui_frame)
        trainer_type_frame.setLayout(QHBoxLayout())
        self._gui_frame.layout().addWidget(trainer_type_frame)

        train_method_type_label = QLabel("Trainer Type:", trainer_type_frame)
        data_frame_combobox = QComboBox(trainer_type_frame)
        data_frame_combobox.setModel(self._data)
        trainer_type_frame.layout().addWidget(train_method_type_label)
        trainer_type_frame.layout().addWidget(data_frame_combobox)
        trainer_type_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Expanding, QSizePolicy.Minimum))
        trainer_type_widget_stack = QStackedWidget(self._gui_frame)
        trainer_type_widget_stack.addWidget(self._data.get_linear_regression_frame())
        trainer_type_widget_stack.addWidget(self._data.get_logistic_regression_frame())
        trainer_type_widget_stack.addWidget(self._data.get_randomforest_regressor_frame())
        trainer_type_widget_stack.addWidget(self._data.get_support_vector_regression_frame())
        data_frame_combobox.currentIndexChanged.connect(lambda index: trainer_type_widget_stack.setCurrentIndex(index))
        self._gui_frame.layout().addWidget(trainer_type_widget_stack)

        self._gui_frame.layout().addItem(QSpacerItem(20, 500, QSizePolicy.Minimum, QSizePolicy.Expanding))

    def get_train_type(self) -> Trainer.Type:
        return self._data_frame_combobox.currentIndex()


class ModelEvaluation(PipeLineProcess):
    def __init__(self, pipeline):
        PipeLineProcess.__init__(self, pipeline)
        super().set_process_type(self.__class__.__name__)
        self._type = None
        super().set_process_type(self.__class__.__name__)
        self.construct_gui()

    def construct_gui(self):
        self._gui_frame = QFrame()
        self._gui_frame.setLayout(QVBoxLayout())
        evaluation_type_frame = QFrame(self._gui_frame)
        evaluation_type_frame.setLayout(QHBoxLayout())
        self._gui_frame.layout().addWidget(evaluation_type_frame)

        evaluation_type_label = QLabel("Evaluation Type:", evaluation_type_frame)
        data_frame_combobox = QComboBox(evaluation_type_frame)
        data_frame_combobox.setModel(Dl_EvaluationMethodModel())
        evaluation_type_frame.layout().addWidget(evaluation_type_label)
        evaluation_type_frame.layout().addWidget(data_frame_combobox)
        evaluation_type_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Expanding, QSizePolicy.Minimum))

        self._gui_frame.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))



class PipeLine:
    def __init__(self):
        self._data_frame_id = -1
        self._processes = [
            DataImport(self),
            TargetValueSelection(self),
            FeatureSelection(self),
            MissingValuesFill(self),
            #Standardization(self),
            ModelTraining(self),
            ModelEvaluation(self)
        ]

    def set_data_frame_id(self, idd: int):
        self._data_frame_id = idd
        for process in self._processes:
            process.set_data_frame_id(idd)

    def get_data_frame_id(self) -> int:
        return self._data_frame_id

    def get_processes(self):
        return self._processes

    def error_check(self):
        print("error")

    def apply(self):
        pass


class PipeLineModel(QStandardItemModel):
    def __init__(self):
        super().__init__()
        self._pipeLines = []
        self.setHorizontalHeaderLabels(['Process', 'Action', 'Status'])

    def add_pipeline(self, pipeline):
        self._pipeLines.append(pipeline)
        processes = pipeline.get_processes()
        pipeline_item = QStandardItem("PipeLine")
        self.setItem(len(self._pipeLines) - 1, 0, pipeline_item)
        for index in range(len(processes)):
            process_item = QStandardItem(processes[index].get_process_type())
            pipeline_item.setChild(index, 0, process_item)
            process_item.setData(processes[index], Qt.UserRole)
