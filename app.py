import sys
from PySide2.QtWidgets import QApplication
from Dl_MainWindow import Dl_MainWindow
import os

if __name__ == '__main__':
    # Create the Qt Application
    if os.name == 'nt':
        import PySide2
        pyqt = os.path.dirname(PySide2.__file__)
        QApplication.addLibraryPath(os.path.join(pyqt, "plugins"))
    app = QApplication(sys.argv)
    # Create and show the form
    mainWindow = Dl_MainWindow()
    mainWindow.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
