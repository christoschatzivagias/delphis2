import pandas


class Imputer:
    def __init__(self):
        self._name = "Abstract"

    def impute(self, feature_name: str, dt: pandas.DataFrame):
        return

    def get_name(self) -> str:
        return self._name


class MeanImputer(Imputer):
    def __init__(self):
        super().__init__()
        self._name = "Mean"

    def impute(self, feature_name: str, dt: pandas.DataFrame):
        return


class MedianImputer(Imputer):
    def __init__(self):
        super().__init__()
        self._name = "Median"

    def impute(self, feature_name: str, dt: pandas.DataFrame):
        return


class ZeroImputer(Imputer):
    def __init__(self):
        super().__init__()
        self._name = "Zero"

    def impute(self, feature_name: str, dt: pandas.DataFrame):
        return


class ImputerContainer:

    imputers = [
        MeanImputer(),
        MedianImputer(),
        ZeroImputer()
    ]

    @staticmethod
    def get_imputer_by_name(name: str) -> Imputer:
        return next(filter(lambda x: x.get_name() == name, ImputerContainer.imputers), None)

    @staticmethod
    def get_names() -> list:
        return list(map(lambda x: x.get_name(), ImputerContainer.imputers))
