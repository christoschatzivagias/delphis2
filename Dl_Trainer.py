import pandas
from enum import Enum


class Trainer:
    class Type:
        LinearRegression = 0
        LogisticRegression = 1
        RandomForestRegressor = 2
        SVRegression = 3

    def __init__(self):
        self._name = "Abstract"

    def train(self, train_data: pandas.DataFrame, train_test: pandas.Series):
        return

    def get_name(self) -> str:
        return self._name


class LinearRegressionTrainer(Trainer):
    class Type(Enum):
        Simple = 0
        Ridge = 1
        Lasso = 2

    def __init__(self):
        super().__init__()
        self._name = "LinearRegression"
        self._type = LinearRegressionTrainer.Type.Simple
        self._alpha = 0.0001

    def set_type(self, type: Type):
        self._type = type

    def set_alpha(self, alpha: int):
        self._alpha = alpha

    def get_types(self):
        return ["Simple", "Ridge", "Lasso"]

    def train(self, train_data: pandas.DataFrame, train_test: pandas.Series) -> object:
        return None


class RandomForestRegressorTrainer(Trainer):
    def __init__(self):
        super().__init__()
        self._name = "RandomForestRegressor"
        self._tree_number = 200

    def train(self, train_data: pandas.DataFrame, train_test: pandas.Series) -> object:
        return None


class SVRegressionTrainer(Trainer):
    def __init__(self):
        super().__init__()
        self._name = "SVRegression"

    def train(self, train_data: pandas.DataFrame, train_test: pandas.Series) -> object:
        return None


class LogisticRegressionTrainer(Trainer):
    class Type(Enum):
        Ridge = 1
        Lasso = 2

    def __init__(self):
        super().__init__()
        self._name = "LogisticRegression"
        self._type = LogisticRegressionTrainer.Type.Lasso

    def set_type(self, type: Type):
        self._type = type

    def get_types(self):
        return ["Ridge", "Lasso"]

    def train(self, train_data: pandas.DataFrame, train_test: pandas.Series) -> object:
        return None


class TrainerContainer:
    trainers = [
        LinearRegressionTrainer(),
        LogisticRegressionTrainer(),
        RandomForestRegressorTrainer(),
        SVRegressionTrainer()
    ]

    @staticmethod
    def get_trainer_by_name(name: str) -> Trainer:
        return next(filter(lambda x: x.get_name() == name, TrainerContainer.trainers), None)

    @staticmethod
    def get_names() -> list:
        return list(map(lambda x: x.get_name(), TrainerContainer.trainers))
