from PySide2.QtWidgets import QMainWindow, QApplication, QFrame, QAction, QHBoxLayout, QVBoxLayout, QSplitter, QComboBox
from PySide2.QtWidgets import QFileDialog, QListView, QTableView, QAbstractItemView, QTabWidget, QMdiArea, QPushButton
from PySide2.QtWidgets import QTreeView, QItemDelegate
from PySide2.QtCore import QStringListModel, Qt, QModelIndex
from Dl_DataFrame import Dl_PandasModel
from PipeLineModel import PipeLineModel, PipeLine, PipeLineProcess
from Dl_DataFrame import Dl_DataFrameCatalog
import PySide2.QtCore
import gc


class DlPipeLineView(QTreeView):
    currentRowChanged = PySide2.QtCore.Signal(PipeLineProcess)

    def __init__(self, parent):
        super().__init__(parent)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)

    def currentChanged(self, current: QModelIndex, previous: QModelIndex):
        process = current.data(Qt.UserRole)
        self.currentRowChanged.emit(process)


class Dl_MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self._currentProcessWidget = None
        self._tabWidget = QTabWidget(self)
        self._dataFrame = QFrame(self._tabWidget)
        self._dataSplitter = QSplitter(self._dataFrame)
        self._dataFrame.setLayout(QHBoxLayout())
        self._dataFrame.layout().addWidget(self._dataSplitter)

        self._leftSideFrame = QFrame(self._dataFrame)
        self._leftSideFrame.setLayout(QVBoxLayout())
        self._leftSideFrame.layout().setContentsMargins(0, 0, 0, 0)
        self._importedFilesList = QListView(self._dataFrame)
        self._importedFilesList.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self._importedFilesList.setModel(Dl_DataFrameCatalog.importedFileModel)
        self._leftSideFrame.layout().addWidget(self._importedFilesList)

        self._deleteButton = QPushButton("Delete", self._leftSideFrame)
        self._deleteButton.clicked.connect(self.delete_data)
        self._leftSideFrame.layout().addWidget(self._deleteButton)

        self._dataFrameMdiArea = QMdiArea(self._tabWidget)
        self._dataFrameMdiArea.setTabsClosable(False)
        self._dataFrameMdiArea.setViewMode(QMdiArea.TabbedView)
        self._dataSplitter.addWidget(self._leftSideFrame)
        self._dataSplitter.addWidget(self._dataFrameMdiArea)
        self._tabWidget.addTab(self._dataFrame, str("Data"))

        self._plotFrame = QFrame(self._tabWidget)
        self._plotFrame.setLayout(QVBoxLayout())
        self._dataFrameComboBox = QComboBox(self._plotFrame)
        self._plotFrameMdiArea = QMdiArea(self._plotFrame)
        self._plotFrameMdiArea.setTabsClosable(False)
        self._plotFrameMdiArea.setViewMode(QMdiArea.TabbedView)
        self._dataFrameComboBox.setModel(Dl_DataFrameCatalog.importedFileModel)
        self._plotFrame.layout().addWidget(self._dataFrameComboBox)
        self._plotFrame.layout().addWidget(self._plotFrameMdiArea)
        self._tabWidget.addTab(self._plotFrame, str("Plot"))

        self._pipeLineSplitter = QSplitter(self._tabWidget)
        self._pipeLineView = DlPipeLineView(self._pipeLineSplitter)
        self._pipeLineModel = PipeLineModel()
        self._pipeLineModel.add_pipeline(PipeLine())
        self._pipeLineView.setModel(self._pipeLineModel)
        self._pipeLineView.expandAll()
        self._pipeLineView.resizeColumnToContents(0)
        self._pipeLineSplitter.addWidget(self._pipeLineView)
        self._pipeLineProcessFrame = QFrame(self._pipeLineSplitter)
        self._pipeLineProcessFrame.setLayout(QVBoxLayout())
        self._pipeLineSplitter.addWidget(self._pipeLineProcessFrame)
        self._tabWidget.addTab(self._pipeLineSplitter, str("Pipeline"))

        self._pipeLineView.currentRowChanged.connect(self.display_gui)

        self.init_ui()

    def display_gui(self, process: PipeLineProcess):
        if process is None:
            return
        if self._currentProcessWidget is not None:
            self._currentProcessWidget.hide()
        self._pipeLineProcessFrame.layout().removeWidget(self._currentProcessWidget)

        self._currentProcessWidget = process.gui_frame()

        if process.gui_frame() is not None:
            self._pipeLineProcessFrame.layout().addWidget(self._currentProcessWidget)
            self._currentProcessWidget.show()

    def delete_data(self):
        index = self._importedFilesList.currentIndex().row()
        if index >= 0:
            Dl_DataFrameCatalog.delete_data(index)
            sub_window_list = self._dataFrameMdiArea.subWindowList()
            self._dataFrameMdiArea.removeSubWindow(sub_window_list[index])
            gc.collect()

    def init_main_menu(self):
        import_csv = QAction("Import CSV", self)
        import_csv.setStatusTip('Import CSV File')
        import_csv.triggered.connect(self.import_csv)

        exit_act = QAction('Exit', self)
        exit_act.setShortcut('Ctrl+Q')
        exit_act.setStatusTip('Exit application')
        exit_act.triggered.connect(QApplication.quit)

        self.statusBar()

        menu_bar = self.menuBar()
        file_menu = menu_bar.addMenu('File')

        file_menu.addAction(import_csv)
        file_menu.addAction(exit_act)

    def import_csv(self):
        try:
            csv_file_name = QFileDialog.getOpenFileName(self, "Select CSV file", filter="*.csv")
            Dl_DataFrameCatalog.append_csv_dataframe(csv_file_name[0])
            latest_index = len(Dl_DataFrameCatalog.catalog)-1
            model = Dl_PandasModel(Dl_DataFrameCatalog.catalog[latest_index]['DataFrame'])
            table_view = QTableView(self._dataFrameMdiArea)
            table_view.setSelectionBehavior(QAbstractItemView.SelectRows)
            table_view.setModel(model)
            table_view.setWindowTitle(Dl_DataFrameCatalog.catalog[latest_index]['Name'])
            self._dataFrameMdiArea.addSubWindow(table_view, Qt.Window | Qt.WindowTitleHint | Qt.WindowMinMaxButtonsHint)
            table_view.show()

        except Exception as e:
            print(e)
            pass

    def init_ui(self):
        self.init_main_menu()
        self.setCentralWidget(self._tabWidget)
        self.setWindowTitle('Delphis')
        self.show()
