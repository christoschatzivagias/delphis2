from PySide2.QtCore import QModelIndex, Qt, QStringListModel
from PySide2.QtSql import QSqlTableModel
from PySide2.QtGui import QStandardItemModel, QStandardItem
from PySide2.QtWidgets import QFrame, QComboBox, QDoubleSpinBox, QLabel, QSpinBox, QGridLayout
from Dl_ValueFiller import ImputerContainer
from Dl_ValueStandardizer import StandardizerContainer
from Dl_Trainer import TrainerContainer
from Dl_ModelEvaluation import EvaluationMethodContainer

import pandas
import os


class Dl_DataFrameCatalog(object):
    def __init__(self):
        super().__init__()
    catalog = []
    importedFileModel = QStringListModel()

    @staticmethod
    def append_csv_dataframe(csv_file):
        Dl_DataFrameCatalog.catalog.append({"Name": os.path.basename(csv_file),
                                            "Path": csv_file,
                                            "DataFrame": pandas.read_csv(csv_file)
                                            })
        imported_files = list(map(lambda file: file['Name'], Dl_DataFrameCatalog.catalog))
        Dl_DataFrameCatalog.importedFileModel.setStringList(imported_files)

    @staticmethod
    def get_path_by_name(self, name):
        for entry in Dl_DataFrameCatalog.catalog:
            if entry['Name'] == name:
                return entry['Path']

        return None

    @staticmethod
    def delete_data(index):
        del Dl_DataFrameCatalog.catalog[index]
        imported_files = list(map(lambda file: file['Name'], Dl_DataFrameCatalog.catalog))
        Dl_DataFrameCatalog.importedFileModel.setStringList(imported_files)


class Dl_DataFrameMissingValuesFillModel(QStandardItemModel):
    def __init__(self):
        super().__init__()
        super().setHorizontalHeaderLabels(['Name', 'Type', 'Missing Values Count', 'Percentage', 'Fill Type', 'Apply'])

    def set_data_frame_id(self, idd: int):
        super().clear()
        super().setHorizontalHeaderLabels(['Name', 'Type', 'Missing Values Count', 'Percentage', 'Fill Type', 'Apply'])
        missing_values_dict = Dl_DataFrameCatalog.catalog[idd]['DataFrame'].isnull().sum().to_dict()
        df_types = Dl_DataFrameCatalog.catalog[idd]['DataFrame'].dtypes.to_dict()
        index = 0
        for feature_name, missing_values_count in missing_values_dict.items():
            name_item = QStandardItem(feature_name)
            name_item.setEditable(False)
            self.setItem(index, 0, name_item)
            type_item = QStandardItem(str(df_types[feature_name]))
            type_item.setEditable(False)
            type_item.setTextAlignment(Qt.AlignHCenter)
            self.setItem(index, 1, type_item)
            missing_values_count_item = QStandardItem(str(missing_values_count))
            missing_values_count_item.setEditable(False)
            missing_values_count_item.setTextAlignment(Qt.AlignHCenter)
            self.setItem(index, 2, missing_values_count_item)
            missing_values_perc_item = QStandardItem(str(round(100*missing_values_count/float(Dl_DataFrameCatalog.catalog[idd]['DataFrame'].shape[0]), 2)) + '%')
            missing_values_perc_item.setEditable(False)
            missing_values_perc_item.setTextAlignment(Qt.AlignHCenter)

            self.setItem(index, 3, missing_values_perc_item)
            fill_type_item = QStandardItem()
            fill_type_item.setData(str(ImputerContainer.get_names()), Qt.DisplayRole)
            fill_type_item.setEditable(False)
            fill_type_item.setTextAlignment(Qt.AlignHCenter)

            self.setItem(index, 4, fill_type_item)
            apply_item = QStandardItem('Fill')
            apply_item.setEditable(False)
            apply_item.setTextAlignment(Qt.AlignHCenter)

            self.setItem(index, 5, apply_item)

            index = index + 1


class Dl_DataFrameFeatureSelectionModel(QStandardItemModel):
    def __init__(self):
        super().__init__()
        super().setHorizontalHeaderLabels(['Name', 'Type', 'Count', 'Mean', "Std", "Min", "25%", "50%", "75%", "Max"])

    def set_data_frame_id(self, idd: int):
        super().clear()
        super().setHorizontalHeaderLabels(['Name', 'Type', 'Count', 'Mean', "Std", "Min", "25%", "50%", "75%", "Max"])

        stats = Dl_DataFrameCatalog.catalog[idd]['DataFrame'].describe().to_dict()
        df_types = Dl_DataFrameCatalog.catalog[idd]['DataFrame'].dtypes.to_dict()

        for index in range(len(Dl_DataFrameCatalog.catalog[idd]['DataFrame'].columns)):
            feature_name = Dl_DataFrameCatalog.catalog[idd]['DataFrame'].columns[index]
            name_item = QStandardItem(feature_name)
            name_item.setEditable(False)
            name_item.setCheckable(True)
            name_item.setCheckState(Qt.Checked)
            self.setItem(index, 0, name_item)
            type_item = QStandardItem(str(df_types[feature_name]))
            type_item.setEditable(False)
            self.setItem(index, 1, type_item)
            if feature_name in stats:
                count_item = QStandardItem(str(round(stats[feature_name]['count'], 2)))
                count_item.setEditable(False)
                self.setItem(index, 2, count_item)

                mean_item = QStandardItem(str(round(stats[feature_name]['mean'], 2)))
                mean_item.setEditable(False)
                self.setItem(index, 3, mean_item)

                std_item = QStandardItem(str(round(stats[feature_name]['std'], 2)))
                std_item.setEditable(False)
                self.setItem(index, 4, std_item)

                min_item = QStandardItem(str(round(stats[feature_name]['min'], 2)))
                min_item.setEditable(False)
                self.setItem(index, 5, min_item)

                twenty_five_item = QStandardItem(str(round(stats[feature_name]['25%'], 2)))
                twenty_five_item.setEditable(False)
                self.setItem(index, 6, twenty_five_item)

                fifty_item = QStandardItem(str(round(stats[feature_name]['50%'], 2)))
                fifty_item.setEditable(False)
                self.setItem(index, 7, fifty_item)

                seventy_five_item = QStandardItem(str(round(stats[feature_name]['75%'], 2)))
                seventy_five_item.setEditable(False)
                self.setItem(index, 8, seventy_five_item)

                max_item = QStandardItem(str(round(stats[feature_name]['max'], 2)))
                max_item.setEditable(False)
                self.setItem(index, 9, max_item)


class Dl_StandardizationModel(QStringListModel):
    def __init__(self):
        super().__init__()
        self.setStringList(StandardizerContainer.get_names())


class Dl_TrainerModel(QStringListModel):
    def __init__(self):
        super().__init__()
        self.setStringList(TrainerContainer.get_names())

    def get_linear_regression_frame(self) -> QFrame:
        main_frame = QFrame()
        main_frame.setLayout(QGridLayout())
        regression_type_label = QLabel("Regularization Type:", main_frame)
        regression_type_combobox = QComboBox(main_frame)
        regression_type_combobox.setModel(QStringListModel(TrainerContainer.get_trainer_by_name("LinearRegression").get_types()))
        main_frame.layout().addWidget(regression_type_label, 0, 0)
        main_frame.layout().addWidget(regression_type_combobox, 0, 1)
        alpha_label = QLabel("Alpha:", main_frame)
        alpha_spinbox = QDoubleSpinBox(main_frame)
        main_frame.layout().addWidget(alpha_label, 1, 0)
        main_frame.layout().addWidget(alpha_spinbox, 1, 1)

        return main_frame

    def get_logistic_regression_frame(self) -> QFrame:
        main_frame = QFrame()
        main_frame.setLayout(QGridLayout())
        regression_type_label = QLabel("Regularization Type:", main_frame)
        regression_type_combobox = QComboBox(main_frame)
        regression_type_combobox.setModel(QStringListModel(TrainerContainer.get_trainer_by_name("LogisticRegression").get_types()))

        strength_label = QLabel("Strength/C:", main_frame)
        strength_spinbox = QDoubleSpinBox(main_frame)
        main_frame.layout().addWidget(regression_type_label, 0, 0)
        main_frame.layout().addWidget(regression_type_combobox, 0, 1)
        main_frame.layout().addWidget(strength_label, 1, 0)
        main_frame.layout().addWidget(strength_spinbox, 1, 1)
        return main_frame

    def get_randomforest_regressor_frame(self) -> QFrame:
        main_frame = QFrame()
        main_frame.setLayout(QGridLayout())
        trees_number_label = QLabel("Number of trees:", main_frame)
        trees_number_spinbox = QSpinBox(main_frame)
        main_frame.layout().addWidget(trees_number_label, 0, 0)
        main_frame.layout().addWidget(trees_number_spinbox, 0, 1)
        return main_frame

    def get_support_vector_regression_frame(self) -> QFrame:
        main_frame = QFrame()
        main_frame.setLayout(QGridLayout())
        cost_label = QLabel("Cost (C):", main_frame)
        cost_spinbox = QDoubleSpinBox(main_frame)
        rl_epsilon_label = QLabel("Regression loss epsilon:", main_frame)
        rl_epsilon_spinbox = QDoubleSpinBox(main_frame)
        main_frame.layout().addWidget(cost_label, 0, 0)
        main_frame.layout().addWidget(cost_spinbox, 0, 1)
        main_frame.layout().addWidget(rl_epsilon_label, 1, 0)
        main_frame.layout().addWidget(rl_epsilon_spinbox, 1, 1)

        return main_frame


class Dl_EvaluationMethodModel(QStringListModel):
    def __init__(self):
        super().__init__()
        self.setStringList(EvaluationMethodContainer.get_names())


class Dl_PandasModel(QSqlTableModel):
    def __init__(self, df=pandas.DataFrame(), parent=None):
        QSqlTableModel.__init__(self, parent=parent)
        self._df = df.copy()

    def to_data_frame(self):
        return self._df.copy()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return None

        if orientation == Qt.Horizontal:
            try:
                return self._df.columns.tolist()[section]
            except (IndexError, ):
                return None
        elif orientation == Qt.Vertical:
            try:
                # return self.df.index.tolist()
                return self._df.index.tolist()[section]
            except (IndexError, ):
                return None

    def data(self, index, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return None

        if not index.isValid():
            return None

        return str(self._df.iloc[index.row(), index.column()])

    def setData(self, index, value, role):
        row = self._df.index[index.row()]
        col = self._df.columns[index.column()]
        if hasattr(value, 'toPyObject'):
            # PyQt4 gets a QVariant
            value = value.toPyObject()
        else:
            # PySide gets an unicode
            d_type = self._df[col].dtype
            if d_type != object:
                value = None if value == '' else d_type.type(value)
        self._df.set_value(row, col, value)
        return True

    def rowCount(self, parent=QModelIndex()):
        return len(self._df.index)

    def columnCount(self, parent=QModelIndex()):
        return len(self._df.columns)

    def flags(self, index=QModelIndex()):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled

    def sort(self, column, order):
        column_name = self._df.columns.tolist()[column]
        self.layoutAboutToBeChanged.emit()
        self._df.sort_values(column_name, ascending=order == Qt.AscendingOrder, inplace=True)
        self._df.reset_index(inplace=True, drop=True)
        self.layoutChanged.emit()
