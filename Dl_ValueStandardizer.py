import pandas
from enum import Enum

class Standardizer:
    class Type(Enum):
        Standard = 0
        MinMax = 1
        MaxAbs = 2
        Robust = 3

    def __init__(self):
        self._name = "Abstract"

    def scale(self, feature_name: str, dt: pandas.DataFrame):
        return

    def get_name(self) -> str:
        return self._name


class StandardScaler(Standardizer):
    def __init__(self):
        super().__init__()
        self._name = "StandardScaler"

    def scale(self, feature_name: str, dt: pandas.DataFrame):
        return


class MinMaxScaler(Standardizer):
    def __init__(self):
        super().__init__()
        self._name = "MinMaxScaler"

    def scale(self, feature_name: str, dt: pandas.DataFrame):
        return


class MaxAbsScaler(Standardizer):
    def __init__(self):
        super().__init__()
        self._name = "MaxAbsScaler"

    def scale(self, feature_name: str, dt: pandas.DataFrame):
        return


class RobustScaler(Standardizer):
    def __init__(self):
        super().__init__()
        self._name = "RobustScaler"

    def scale(self, feature_name: str, dt: pandas.DataFrame):
        return


class StandardizerContainer:

    standardizers = [
        StandardScaler(),
        MinMaxScaler(),
        MaxAbsScaler(),
        RobustScaler()
    ]

    @staticmethod
    def get_standardizer_by_name(name: str) -> Standardizer:
        return next(filter(lambda x: x.get_name() == name, StandardizerContainer.standardizers), None)

    @staticmethod
    def get_names() -> list:
        return list(map(lambda x: x.get_name(), StandardizerContainer.standardizers))
