import pandas
from enum import Enum


class EvaluationMethod:
    class Type(Enum):
        CrossValidation = 0
        LeaveOneOut = 1

    def __init__(self):
        self._name = "Abstract"

    def perform(self, train_data: pandas.DataFrame, train_test: pandas.Series):
        return

    def get_name(self) -> str:
        return self._name


class CrossValidation(EvaluationMethod):

    def __init__(self):
        super().__init__()
        self._name = "CrossValidation"
        self._folds = 10

    def set_type(self, folds: int):
        self._folds = folds

    def perform(self, train_data: pandas.DataFrame, train_test: pandas.Series) -> object:
        return None


class LeaveOneOut(EvaluationMethod):

    def __init__(self):
        super().__init__()
        self._name = "LeaveOneOut"

    def perform(self, train_data: pandas.DataFrame, train_test: pandas.Series) -> object:
        return None


class EvaluationMethodContainer:
    evaluationMethods = [
        CrossValidation(),
        LeaveOneOut()
    ]

    @staticmethod
    def get_evaluation_method_by_name(name: str) -> EvaluationMethod:
        return next(filter(lambda x: x.get_name() == name, EvaluationMethodContainer.evaluationMethods), None)

    @staticmethod
    def get_names() -> list:
        return list(map(lambda x: x.get_name(), EvaluationMethodContainer.evaluationMethods))
